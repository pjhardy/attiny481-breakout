# ATtiny841 breakout

![Board render](breakout-render.png)

This project contains KiCad schematics and board layout for a breakout board
for the Atmel ATtiny841 microcontroller.

The board uses an onboard 16MHz crystal, and includes an ICSP header. There
is no voltage regulator, and requires a regulated 5V supply. All MCU pins
are broken out - refer to the [datasheet](http://www.atmel.com/Images/Atmel-8495-8-bit-AVR-Microcontrollers-ATtiny441-ATtiny841_Datasheet.pdf) for pinouts.

## Obtaining

For the ultra-lazy, this board is already available from [breadboardkiller](https://breadboardkiller.com.au): https://breadboardkiller.com.au/designs/529-attiny841-breakoutzip

## Using

### Arduino IDE notes

This board works great with the [SpenceKonde ATtiny core](https://github.com/SpenceKonde/ATTinyCore).
I had issues convincing the chip to use the external clock. This was resolved
by selecting the 16MHz external clock, and then using the "Burn Bootloader"
menu option. That ensured that all of the fuses were set correctly, and
the chip starts using the external clock for all future uploads.

### Bus Pirate hookup:

GND | brown | GND | 14
VCC | orange (5V) | VCC | 1
CS  | white | RESET | 4
MOSI | grey | MOSI | 7
MISO | black | MISO | 8
CLK | purple | SCK | 9

## License

![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
