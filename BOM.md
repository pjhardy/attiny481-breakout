# Bill of materials

* **IC1**: ATTiny841 - SOIC14.
* **Y1**: 16MHz crystal in PTH 11.05x4.65 housing.
* **C1**: 1604 capacitor, 100nF.
* **C2, C3**: 1604 capacitors, 18pF (should match load capacitance of crystal).
* **R1**: 1604 resistor, 100K.
* **SW1**: Tactile switch, I'm using a B3S-1000.
